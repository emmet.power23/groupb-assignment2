#!/bin/bash
ip addr del 10.0.0.1/8 dev h1-eth0
ip link add link h1-eth0 name h1-eth0.56 type vlan id 56
ip addr add 10.0.0.1/8 dev h1-eth0.56
ip link set dev h1-eth0.56 up