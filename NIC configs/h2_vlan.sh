#!/bin/bash
ip addr del 10.0.0.2/8 dev h2-eth0
ip link add link h2-eth0 name h2-eth0.56 type vlan id 56
ip addr add 10.0.0.2/8 dev h2-eth0.56
ip link set dev h2-eth0.56 up