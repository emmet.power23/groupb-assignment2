#!/bin/bash
ip addr del 10.0.0.4/8 dev h4-eth0
ip link add link h4-eth0 name h4-eth0.443 type vlan id 443
ip addr add 10.0.0.4/8 dev h4-eth0.443
ip link set dev h4-eth0.443 up