#!/bin/bash
ip addr del 10.0.0.3/8 dev h3-eth0
ip link add link h3-eth0 name h3-eth0.443 type vlan id 443
ip addr add 10.0.0.3/8 dev h3-eth0.443
ip link set dev h3-eth0.443 up