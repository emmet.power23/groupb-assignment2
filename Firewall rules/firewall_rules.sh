#!/bin/bash
curl -X POST -d '{"nw_src": "10.0.0.0/8", "nw_proto": "ICMP"}' http://localhost:8080/firewall/rules/0000000000000001/56
curl -X POST -d '{"nw_dst": "10.0.0.0/8", "nw_proto": "ICMP"}' http://localhost:8080/firewall/rules/0000000000000001/56
curl -X POST -d '{"nw_src": "10.0.0.0/8", "nw_proto": "TCP"}' http://localhost:8080/firewall/rules/0000000000000001/56
curl -X POST -d '{"nw_dst": "10.0.0.0/8", "nw_proto": "TCP"}' http://localhost:8080/firewall/rules/0000000000000001/56
curl -X POST -d '{"nw_src": "10.0.0.0/8"}' http://localhost:8080/firewall/rules/0000000000000001/443
curl -X POST -d '{"nw_dst": "10.0.0.0/8"}' http://localhost:8080/firewall/rules/0000000000000001/443
curl -X POST -d '{"nw_src": "10.0.0.0/8", "nw_proto" : "ICMP", "actions" : "DENY"}' http://localhost:8080/firewall/rules/0000000000000001/443